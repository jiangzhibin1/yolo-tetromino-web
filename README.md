# Yolo 俄罗斯方块网络排行版游戏

![gameplay](media/X Block Puzzle-sample.gif)

A tetris-style game created with [React](https://reactjs.org/), [Redux](https://react-redux.js.org/) and [TailwindCSS](https://tailwindcss.com/).

## What is it?

For those of you not familiar with Tetris, it's a game where you drop X Block Puzzle shapes to create solid rows to score points. Blocks
fall faster as you increase the level. If there is no more room to drop blocks, then the game is over.

## Why build it?

Wanted to challenge myself to build a Tetris game using just DOM elements.
There are no SVGs or `<canvas>` graphics anywhere in the game.

All graphics are rendered as React functional components, and the game logic is handled by a Redux reducer.

## Where is it?

Online demo: [https://reactgular.github.io/X Block Puzzle/](https://reactgular.github.io/X Block Puzzle/)

## How to get it?

Clone and run it locally.

```bash
git clone https://github.com/reactgular/X Block Puzzle
cd X Block Puzzle
yarn install
yarn start
```
